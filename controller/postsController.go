package controller

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"html/template"
	"io"
	"startecho/const"
	"startecho/db/dao"
	"startecho/handler"
)


// レイアウト適用済のテンプレートを保存するmap
var templates map[string]*template.Template

// Template はHTMLテンプレートを利用するためのRenderer Interfaceです。
type Template struct {
	templates *template.Template
}

// Render はHTMLテンプレートにデータを埋め込んだ結果をWriterに書き込みます。
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return templates[name].ExecuteTemplate(w, "layout.html", data)
}

// 初期化を行います。
func init() {
	loadTemplates()
	dao.MigrateMembers()
	//dao.CreateMember()
	dao.MigratePosts()
}

// 各HTMLテンプレートに共通レイアウトを適用した結果を保存します（初期化時に実行）。
func loadTemplates() {
	templates = make(map[string]*template.Template)
	templates["update_post_form"] = template.Must(
		template.ParseFiles(_const.BaseTemplate, _const.UpdataPost))
	templates["posts_all"] = template.Must(
		template.ParseFiles(_const.BaseTemplate, _const.PostsAll))
}

func MethodOverride(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Request().Method == "POST" {
			method := c.Request().PostFormValue("_method")
			if method == "PUT" || method == "PATCH" || method == "DELETE" {
				c.Request().Method = method
			}
		}
		//fmt.Println(c.Request())
		return next(c)
	}
}

func ControlPosts() *echo.Echo {

	e := echo.New()
	e.Pre(MethodOverride)

	//　ミドルウェアを設定
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())


	// テンプレートを利用するためのRendererの設定
	t := &Template{
		templates: template.Must(template.ParseGlob(_const.BaseTemplate)),
	}
	e.Renderer = t

	e.GET("/", handler.HandleGetPosts)
	e.POST("/posts", handler.HandlePostPost)
	e.POST("/post/edit", handler.HandleEditPost)
	e.POST("/post/update", handler.HandleUpdatePost)
	e.POST("/post/delete", handler.HandleDeletePost)
	//e.PUT("/post/:id", handler.HandleUpdatePost)
	//e.DELETE("/post/:id", handler.HandleDeletePost)

	return e
}
